set ts=2
set sw=2
set expandtab
set textwidth=120
set fo-=l  " Allow wrapping of long lines
set spell
