set ts=2
set sw=2
set expandtab
set foldmethod=syntax
set foldlevel=0
set foldlevelstart=0
