set ts=3
set sw=3
set expandtab
set textwidth=120
set fo-=l  " Allow wrapping of long lines
set spell
