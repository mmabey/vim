Commands for My Setup of Vim
============================

Open and close a fold:
za

Window Splits
Vertical Split : Ctrl+w + v
Horizontal Split: Ctrl+w + s
Close current windows: Ctrl+w + q
Move the windows around: Ctrl+w + <movement>

To-Do List
View the list: <leader>td
Close the list: q
Jump to item selected: Enter

Revision History (Gundu)
<leader>g

Show Python module documentation
<leader>pw

Buffers
Open a file:  :e <filename>
View list of buffers:  :buffers
Switch to buffer #:  :b<number>
Match name of buffer to open:  :b <partial filename><tab>
Close a buffer:  :bd or :bw


Note on the Command-T plugin: Must be installed manually
Command-T: Fuzzy Text File Search
Find file:  <leader>t
Find file from only opened buffers:  <leader>b

Toggle Project File Browser:  <leader>n

"Re-flow" a paragraph:  gqip

Jump to next/prev git change: [c ]c
Stage/unstage/preview hunk: <leader>hs  <leader>hu  <leader>hp

Spelling
To turn off spell checking, enter the command: `set nospell`
`]s` -> go to next misspelled word
`[s` -> go to prev misspelled word
`z=` -> show list of suggestions
`zg` -> add word to custom list
`zw` -> remove word from custom list

To start a new CoVim server: `:CoVim start [port] [name]` (or, from the command line: ./CoVimServer.py [port])
To connect to a running server: `:CoVim connect [host address / 'localhost'] [port] [name]`
To disconnect: `:CoVim disconnect`
To quit Vim while CoVim is connected: `:CoVim quit` or `:qall!`

Open/close quickfix window:  :cope[n]  :ccl[ose]
For more commands/info: http://vimdoc.sourceforge.net/htmldoc/quickfix.html

Save current session:  :mksession <session file name>
Restore saved session:  :source <session file name>
Open vim with session:  vim -S
