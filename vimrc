" Vim-Plug plugins section
call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }  " Directory exploring in a separate pane inside vim
Plug 'scrooloose/nerdcommenter'  " Advanced commenting commands
Plug 'airblade/vim-gitgutter'  " Show git changes in the gutter (left of line numbers)
Plug 'vim-airline/vim-airline'  " Cool status line
Plug 'sjl/badwolf'  " Color scheme
Plug 'tpope/vim-fugitive'  " Awesome git actions inside vim
" Plug 'scrooloose/syntastic'  " Syntax checking for all sorts of languages (requires individual checkers to be installed for each language)
" Plug 'majutsushi/tagbar', {'do': 'sudo apt-get install exuberant-ctags'}  " Side bar with list of tags for the current file
" Plug 'FredKSchott/CoVim', { 'on': 'CoVim', 'do': 'sudo pip install twisted argparse' }  " Collaborative editing!! :D  Make sure twisted and argparse are installed for default Python
Plug 'nathanaelkane/vim-indent-guides'  " Show indentation guide
" Plug 'Yggdroot/indentLine'  " Indentation guide (lines)
Plug 'raimondi/delimitmate'  " Auto-delimit parentheses, quotes, etc.
Plug 'lervag/vimtex'  " Lots of LaTeX support
" Plug 'valloric/youcompleteme', {'do': 'sudo apt-get -y install build-essential cmake python-dev python3-dev && cd ~/.vim/plugged/youcompleteme && ./install.py --clang-completer '}  " Crazy auto-completion -- requires a compiled part too. See instructions below.
Plug 'csexton/trailertrash.vim'  " Highlights tailing whitespace. Enter command :TrailerTrim to strip all trailing whitespace from a file
" End Vim-Plug plugins section
call plug#end()

" Installation instructions for YouCompleteMe:
" sudo apt-get install build-essential cmake python-dev python3-dev
" cd ~/.vim/plugged/youcompleteme
" ./install.py --clang-completer

syntax enable
filetype plugin indent on  " Turn on filetype detection, including plugins for specific file types and indentation rules

" let mapleader="\<Space>"  " Map <leader> to space bar


" Shift-tab to unindent a line. From http://stackoverflow.com/questions/4766230/map-shift-tab-in-vim-to-inverse-tab-in-vim
" for command mode
nnoremap <S-Tab> <<
" for insert mode " TODO: Needs to be tuned a bit
inoremap <S-Tab> <C-d>

" j and k move by screen lines instead of file lines
nnoremap <leader>j j
nnoremap j gj
vnoremap <leader>j j
vnoremap j gj
nnoremap <leader>k k
nnoremap k gk
vnoremap <leader>k k
vnoremap k gk

" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif


colorscheme badwolf


" Show list of tags (tagbar plugin) with F8
nmap <F8> :TagbarToggle<CR>

set autoindent
set autoread  " Reload a file if it was modified outside of vim
set background=dark
set backspace=indent,eol,start
set complete=.,w,k,b,u,t,i
set concealcursor=c
set cursorline  " Highlight current line"
set encoding=utf8
set foldmethod=syntax
if v:version > 703 || v:version == 703 && has("patch541")
  set formatoptions+=j " Delete comment character when joining commented lines
endif
set hlsearch  " Highlight all matches to a search. Un-highlight them with Ctrl+L
set ignorecase
set incsearch
set laststatus=2
" set list listchars=tab:\|\ ,trail:⌴,nbsp:%,extends:▶,precedes:◀
set nocompatible
set number  " Always show line numbers
set scrolljump=1
set scrolloff=5  " Min lines to keep above/below cursor
if &shell =~# 'fish$' && (v:version < 704 || v:version == 704 && !has('patch276'))
  set shell=/bin/bash
endif
set shiftwidth=2
set showmatch
set smartcase
set smarttab
set softtabstop=2
set spell spelllang=en_us  " Set spelling language to American English
set nospell " Don't spell check by default
set splitbelow
set splitright
" Recommended settings for Syntastic
set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
" Allow color schemes to do bright colors without forcing bold.
if &t_Co == 8 && $TERM !~# '^linux\|^Eterm'
  set t_Co=16
endif
set tabstop=2
set visualbell
set wildmenu

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Toggle line comment with nerdcommenter
nmap <leader>/ <plug>NERDCommenterToggle
xmap <leader>/ <plug>NERDCommenterToggle
let g:NERDSpaceDelims = 1  " Add spaces after comment delimiters by default
let g:NERDCompactSexyComs = 1  " Use compact syntax for prettified multi-line comments
let g:NERDCommentEmptyLines = 1  " Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDTrimTrailingWhitespace = 1  " Enable trimming of trailing whitespace when uncommenting


" Default settings for CoVim
let CoVim_default_name = "MikeM"
let CoVim_default_port = "8965"


" Automatically populate the g:airline_symbols dictionary with the powerline symbols
" Need to install fonts for this to look good:
" https://powerline.readthedocs.io/en/master/installation/linux.html#fonts-installation
let g:airline_powerline_fonts = 1
" Expected file type is utf-8[unix] -> no need to show if that's what it is
let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'


" Turn off continuous mode for vimtex compilation
" let g:vimtex_latexmk_continuous = 0
" Run compliation in the background (only necessary when continuous mode is off)
let g:vimtex_latexmk_background = 1
" Don't open the quickfix window if there are only warnings
let g:vimtex_quickfix_open_on_warning = 0


" Show to-do list
map <leader>td <Plug>TaskList


" Tab completion and documentation
" au FileType python set omnifunc=pythoncomplete#Complete
" let g:SuperTabDefaultCompletionType = "context"
" set completeopt=menuone,longest,preview

" Toggle project file tree browser
map <leader>n :NERDTreeToggle<CR>


" Make indentLine show indent on first level
" let g:indentLine_showFirstIndentLevel = 1

" Enable indent guides by default
let g:indent_guides_enable_on_vim_startup = 1


" Change GitGutter's highlight colors
highlight GitGutterAdd    guifg=#000000 guibg=#009900 ctermfg=0 ctermbg=2
highlight GitGutterChange guifg=#000000 guibg=#bbbb00 ctermfg=0 ctermbg=3
highlight GitGutterDelete guifg=#000000 guibg=#ff2222 ctermfg=0 ctermbg=1
highlight GitGutterChangeDelete guifg=#000000 guibg=#bbbb00 ctermfg=0 ctermbg=3
let g:gitgutter_sign_removed = '◣'
let g:gitgutter_sign_modified_removed = '~▶'
